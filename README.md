
# Repository for the INSULAB SiPMs characterization with FERS. 

Please feel free to work on the scripts and to make changes (and check for errors); mind to ***always*** **commit** your changes in a way that does not make Jesus cry, in order to not add chaos to the already existent chaos. 

### Data Repository
**This is mandatory**: to work in peace, clone this repository and put the data in the folder [`data`](./data). Don't touch nomi a caso. 
For people with a CERN account and lxplus: [/eos/project/i/insulab-como/labData/202303_FERS](https://cernbox.cern.ch/files/spaces/eos/project/i/insulab-como/labData/202303_FERS?items-per-page=100)
Alternatively, the “insuread” account can also be used.

Otherwise, check [non-CERN users](https://cernbox.cern.ch/s/oGCwQiCUpHvvKGH).

#### Set UP informations 
Informations can be found in the [logbookSiPMFERS2023](https://docs.google.com/document/d/1_opKxDedsNfBW4tHZNG9rqb5TYHAN1ifkMBbc_MxIcA/edit).

### Philisophy of the data analysis 
The idea is to fully characterized the SensL SiPMs matrix (Beatles) and the Hamamatsu (Chri) SiPMs at the bais voltages of interest. 
- **Step 1** : 
    Acquire systematic dark runs at different voltages in order to count the single photon. 
    NOTE: FERS acquire either with High and Low Gain (values settable in Janus) 

    - **TO DO** 
        Open the low gain data, look at it and find the sigle photon. ***Enjoy!***
        
- **Step 2**:
    Acquire data with the led on at the same voltages with different gain in order to find the correspondence between single and multi photons. 
    
- **Step 3**
    Going to Alessia and have fun with the big laser.
    
We would like to understand at how many photons the SiPMs saturates. 

This README should be modified with _to do_ lists and progress in the analysis.  



