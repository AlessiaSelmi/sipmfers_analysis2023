# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 16:49:30 2023

@author: utente
"""

#%%
%matplotlib Qt5
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os
from parsingBinary import *

#%% 
numCh = 1
ch = 0
sogliola = 160
#%% Dark -- Run 1140

data = openBinary(1140, numCh)

fig, ax = plt.subplots()
fig.set_size_inches(12, 6)
fig.suptitle(f"Run  - LG")

# Istogrammo i dati
    
vect = data["ch_data"]["lg_pha"]
h, bins = np.histogram(vect, range = (0, 8192), bins = 8192)
binc = bins[:-1] + (bins[1]-bins[0])/2

# Li plotto
ax.plot(binc, h, ds = "steps-mid")
ax.axvline(sogliola, c = 'gray', ls = '--')
ax.set_xlabel("Energy [ADC]")
ax.set_ylabel("Counts")
ax.set_title(f"CH{ch}")
ax.set_yscale("log")
ax.grid()

plt.show()

fig, ax = plt.subplots()
fig.set_size_inches(12, 6)

fig.suptitle(f"Run - HG")

# xfit 250 --> 550
# Istogrammo i dati
vect = data["ch_data"]["hg_pha"]
h, bins = np.histogram(vect, range = (0, 8192), bins = 8192)
binc = bins[:-1] + (bins[1]-bins[0])/2

# Li plotto
ax.plot(binc, h, ds = "steps-mid")

ax.set_xlim(0,2000)
ax.set_xlabel("Energy [ADC]")
ax.set_ylabel("Counts")
ax.set_title(f"CH{ch}")
ax.set_yscale("log")
ax.grid()

    
plt.show()
    
d_p2p = 64 # TODO per davvero!


#%% Funzioni 


def Gauss(x, a, mu, sigma):
    return a*np.exp(-(x-mu)**2/(2*sigma**2))


def fittaCose(nRun, sogliola = 230, sigma_left= 2, sigma_right = 2 , nbin = 8192):
    
    data = openBinary(nRun, numCh)


    fig, ax = plt.subplots()
    fig.set_size_inches(12, 6)
    fig.suptitle(f"Run {nRun} - LG")

    # Istogrammo i dati
    
    vect = data["ch_data"]["lg_pha"]
    h, bins = np.histogram(vect, range = (0, 8192), bins = nbin)
    binc = bins[:-1] + (bins[1]-bins[0])/2

    a = np.max(vect[vect>sogliola])
    mu = np.mean(vect[vect>sogliola])
    sigma = np.std(vect[vect>sogliola])
    
    fpara = [a,mu,sigma]
    
    x_fit = binc[(binc>mu-sigma_left*sigma) & (binc<mu+sigma_right*sigma)]
    y_fit = h[(binc>mu-sigma_left*sigma) & (binc<mu+sigma_right*sigma)]
    y_fit = y_fit[(x_fit>sogliola)  & (x_fit<7977)]
    x_fit = x_fit[(x_fit>sogliola)  & (x_fit<7977)]
    # Li plotto
    ax.plot(binc, h, ds = "steps-mid", label = 'Data')
    ax.plot(x_fit, y_fit,  ds = "steps-mid", label = 'Data to fit')
    ax.axvline(sogliola, c = 'gray', ls = '--')

    oP, pC = opt.curve_fit(Gauss, x_fit, y_fit, sigma = np.sqrt(y_fit), p0 = fpara)
    
    ax.plot(x_fit, Gauss(x_fit, *oP), label = "Fit ", c = 'black', ls = '--')
    ax.plot(oP[1], Gauss(oP[1],*oP), '*', label = f"mu = {oP[1]:.2f}", c = 'lime', ms = 11)
    ax.set_xlabel("Energy [ADC]")
    ax.set_ylabel("Counts")
    ax.set_title(f"CH{ch}")
    ax.set_yscale("log")
    ax.grid()
    ax.set_xlim(0,mu+5*sigma)
    ax.legend()
    
    plt.show()
    
    oP_list.append(oP)
    pC_list.append(pC)
    
    return 

def MediaCose(nRun, sogliola = 230,  nbin = 8192):
    data = openBinary(nRun, numCh)


    fig, ax = plt.subplots()
    fig.set_size_inches(12, 6)
    fig.suptitle(f"Run {nRun} - LG")

    # Istogrammo i dati
    
    vect = data["ch_data"]["lg_pha"]
    h, bins = np.histogram(vect, range = (0, 8192), bins = nbin)
    binc = bins[:-1] + (bins[1]-bins[0])/2

    media = np.mean(vect[vect>sogliola])
    sigma = np.std(vect[vect>sogliola])
    
    

    # Li plotto
    ax.plot(binc, h, ds = "steps-mid", label = 'Data')
    ax.axvline(media, label = f"Mean = {media:.2f}", c = 'gray', ls = '--')
    ax.axvline(sogliola, c = 'gray', ls = '--')

    
    ax.set_xlabel("Energy [ADC]")
    ax.set_ylabel("Counts")
    ax.set_title(f"CH{ch}")
    ax.set_yscale("log")
    ax.grid()
    ax.set_xlim(0,media+5*sigma)
    ax.legend()
    
    media_list.append(media)
    media_err_list.append(sigma/np.sqrt(len(vect)))
    
    plt.show()

#%%

def GuardaCose(nRun, sogliola, sigma_left= 2, sigma_right = 2, nbin = 8192):
    
    data = openBinary(nRun, numCh)


    fig, ax = plt.subplots()
    fig.set_size_inches(12, 6)
    fig.suptitle(f"Run {nRun} - LG")

    # Istogrammo i dati
    
    vect = data["ch_data"]["lg_pha"]
    h, bins = np.histogram(vect, range = (0, 8192), bins = nbin)
    binc = bins[:-1] + (bins[1]-bins[0])/2

    a = np.max(vect[vect>sogliola])
    mu = np.mean(vect[vect>sogliola])
    sigma = np.std(vect[vect>sogliola])
        
    
    x_fit = binc[(binc>mu-sigma_left*sigma) & (binc<mu+sigma_right*sigma)]
    y_fit = h[(binc>mu-sigma_left*sigma) & (binc<mu+sigma_right*sigma)]
    y_fit = y_fit[(x_fit>sogliola)  & (x_fit<7978)]
    x_fit = x_fit[(x_fit>sogliola)  & (x_fit<7978)]

    # Li plotto
    ax.plot(binc, h, ds = "steps-mid", label = 'Data')
    ax.plot(x_fit, y_fit,  ds = "steps-mid", label = 'Data to fit')
    ax.axvline(sogliola, c = 'gray', ls = '--')

    ax.set_xlabel("Energy [ADC]")
    ax.set_ylabel("Counts")
    ax.set_title(f"CH{ch}")
    ax.set_yscale("log")
    ax.grid()
    ax.set_xlim(0,mu+5*sigma)
    ax.legend()
    
    plt.show()
    
    return 

GuardaCose(nRun_list[11], 230)
#%%
do_list_tarocca = [3.9, 3.6, 3.3, 3, 2.7, 2.4, 2.5, 2.2, 2, 2.1, 1.8, 1.7, 2, 1.8]
do_list = [3.9, 3.6, 3.3, 3, 2.7, 2.4, 2.5, 2.2, 2, 2.1, 1.9, 1.8, 2, 1.9]
nRun_list = [1156, 1157, 1158, 1159, 1160, 1161, 1162, 1163, 1164, 1165, 1166, 1167, 1168, 1169]

#%%

media_list = []
media_err_list = []

for Run in nRun_list:
    MediaCose(Run)
    
    
#%%
oP_list = []
pC_list = []

# Run1156, DO 3.9
fittaCose(nRun_list[0],sogliola = 160)

# Run1157, DO  3.6
fittaCose(nRun_list[1],sogliola = 170)

# Run1158, DO 3.3
fittaCose(nRun_list[2])

# Run1159, DO 3
fittaCose(nRun_list[3])

# Run1160, DO 2.7
fittaCose(nRun_list[4], nbin = 4000)

# Run1161, DO 2.4
fittaCose(nRun_list[5], nbin = 4000)

# Run1162, DO 2.5
fittaCose(nRun_list[6], nbin = 2000)

# Run1163, DO 2.2
fittaCose(nRun_list[7], nbin = 1500, sigma_left = 0.5)

# Run1164, DO 2
fittaCose(nRun_list[8], nbin = 1500, sigma_left = 0.5)

# Run1165, DO 2.1
fittaCose(nRun_list[9], nbin = 1500, sigma_left = 0.2)

# Run1166, DO 1.9
fittaCose(nRun_list[10], nbin = 1000, sigma_left =0, sigma_right =1.6)


# Run1167, DO 1.8 -- saturazione
fittaCose(nRun_list[11], nbin = 1000, sigma_left = -0.1, sigma_right =1.5)

# Run1168, DO 2
fittaCose(nRun_list[12], nbin = 1000, sigma_left = 0, sigma_right = 1.6)


# Run1169, DO 1.9
fittaCose(nRun_list[13], nbin = 1000, sigma_left = 0, sigma_right = 1.6)

mu = np.array([oP_list[i][1] for i in range(len(oP_list))])
err_mu = np.array([np.sqrt(np.diag(pC_list[i]))[1] for i in range(len(pC_list))])
#%%


# Run1169, DO 1.9
fittaCose(nRun_list[13], nbin = 1000, sigma_left = 0, sigma_right = 1.6)




#%%

def myLine(x, m, q):
    return m*x+q

offset = 62 # TODO :) 

x_fit = 10**(-1*np.array(do_list))[np.array(do_list)>=2]
x = 10**(-1*np.array(do_list))
y_fit = (mu-offset)[np.array(do_list)>=2]
oP, pC = opt.curve_fit(myLine,x_fit, y_fit, sigma = err_mu[np.array(do_list)>=2])


fig, ax = plt.subplots()
fig.set_size_inches(12, 6)
fig.suptitle("DO vs Peak")


ax.plot(10**(-1*np.array(do_list)), mu-offset, '*')
ax.plot(x, myLine(x, *oP))
ax.set_xlabel('Densità Ottiche')
ax.set_ylabel('Picco [ADC]')
ax.grid()



x_fit = 10**(-1*np.array(do_list_tarocca))
x = 10**(-1*np.array(do_list_tarocca))
y_fit = (mu-offset)
oP, pC = opt.curve_fit(myLine,x_fit, y_fit, sigma = err_mu)


fig, ax = plt.subplots()
fig.set_size_inches(12, 6)
fig.suptitle("DO vs Peak")


ax.plot(10**(-1*np.array(do_list_tarocca)), mu-offset, '*')
ax.plot(x, myLine(x, *oP))
ax.set_xlabel('Densità Ottiche')
ax.set_ylabel('Picco [ADC]')
ax.grid()

#%%

media = np.array(media_list)
media_err = np.array(media_err_list)


x_fit = 10**(-1*np.array(do_list))[np.array(do_list)>=2]
x = 10**(-1*np.array(do_list))
y_fit = (media-offset)[np.array(do_list)>=2]
oP, pC = opt.curve_fit(myLine,x_fit, y_fit, sigma = media_err[np.array(do_list)>=2])


fig, ax = plt.subplots()
fig.set_size_inches(12, 6)
fig.suptitle("DO vs Peak")


ax.plot(10**(-1*np.array(do_list)), mu-offset, '*')
ax.plot(x, myLine(x, *oP))
ax.set_xlabel('Densità Ottiche')
ax.set_ylabel('Picco [ADC]')
ax.grid()



x_fit = 10**(-1*np.array(do_list_tarocca))
x = 10**(-1*np.array(do_list_tarocca))
y_fit = (media-offset)
oP, pC = opt.curve_fit(myLine,x_fit, y_fit, sigma = media_err)


fig, ax = plt.subplots()
fig.set_size_inches(12, 6)
fig.suptitle("DO vs Peak")


ax.plot(10**(-1*np.array(do_list_tarocca)), mu-offset, '*')
ax.plot(x, myLine(x, *oP))
ax.set_xlabel('Densità Ottiche')
ax.set_ylabel('Picco [ADC]')
ax.grid()
# %%
