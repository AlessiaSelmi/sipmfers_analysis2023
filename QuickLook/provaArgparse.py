#!/usr/bin/env python3

import argparse


parser = argparse.ArgumentParser(
                    prog='ProgramName',
                    description='What the program does',
                    epilog='Text at the bottom of help')


parser.add_argument("filename")           # positional argument
parser.add_argument("-c", "--channel", 
		dest = "channelNumber", default = 0,
		help = "Numero di canale da plottare")
parser.add_argument("-l", "--length", 
		dest = "dataLength", default = 1,
		help = "Numero totale di canali acquisiti")

args = parser.parse_args()


print(f"Numero del file da plottare: {args.filename}")
print(f"Numero canale da plottare: {args.channelNumber}")
print(f"Numero totale di canali acquisiti: {args.dataLength}")

if args.channelNumber >= args.dataLength:
	print(f"ERRORE PROIBITO!!! ")
