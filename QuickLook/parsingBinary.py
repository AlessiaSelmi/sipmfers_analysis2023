#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 31 09:15:14 2023

@author: utente
"""

import numpy as np
from matplotlib import pyplot as plt

import sys, os, re, glob

from datetime import datetime as dt

#%% 





def openBinary(nRun, numChannel):

    # File header according to the Janus software manual pag 38
    #
    # Binary file structure memo:
    # 
    # [FILE HEADER] +
    #   nevents x 
    #            [EVENT HEADER] + 
    #            64 x [DATA]


    # Common to all acquisition modes:

    Janus_file_header = np.dtype(
        [
            ("data_format_version", ("B", 2)),  # 16 bits, Data Format Version
            ("software_version", ("B", 3)),  # 24 bits, Data Format Version
            ("acq_mode", np.dtype("B")),  # 8  bits, Acq Mode
            ("time_unit", np.dtype("B")),  # 8  bits, Time Unit # new v3
            ("energy_n_channels",np.dtype(">u2")),  # 16  bits, Energy NChannels # new v3
            ("time_conversion", np.dtype("<f4")), # 32 bits, Time Conversion # new v3
            ("acq_start", np.dtype("u8"))  # 64 bits, Start Acquisition
        ]
    )

    # Acquisition modes dict (just to print them)

    acq_modes = {
        1: "Spectroscopy mode",
        2: "Timing Mode",
        3: "Spectroscopy + Timing Mode",
        4: "Counting Mode"
    }

    # Dictionaries with the 4 different event schema
    # TODO: it would be convenient to put it in an
    # external file (json)

    # before, the event schema to be repeated 64 times

    Janus_event_data_schema = {
        1: np.dtype(  # data for event in spectroscopy mode
            [
                ("channel_id", np.dtype("B")),
                ("data_type", np.dtype("B")),
                ("lg_pha", np.int16),
                ("hg_pha", np.int16),
            ]
        )
    }

    # now the general event schema
    # NOTA: Rispetto a valerio, noi salviamo solo 4 matrici

    Janus_event_schema = {
        1: np.dtype(
            [
                ("event_size", np.dtype("u2")),
                ("board_id", np.dtype("B")),
                ("trigger_time_stamp", np.dtype("f8")),
                ("trigger_id", np.dtype("u8")),
                ("channel_mask", np.dtype("u8")),
                ("ch_data", (Janus_event_data_schema[1], numChannel)),  # 64 times the data schema
            ]
        )
    }
    
    input_file_name = f"./../data/Run{nRun}_list.dat"
    
    with open(input_file_name, "rb") as fp:
        

        # File header, 1 time
        head = np.fromfile(fp, dtype=Janus_file_header, count=1)

        #print_file_info(head)

        # Events, count=-1 makes it run until the end of the file
            
        acq_mode = head["acq_mode"][0]
        data = np.fromfile(fp, dtype=Janus_event_schema[acq_mode], count=-1)
        
        return data
    
