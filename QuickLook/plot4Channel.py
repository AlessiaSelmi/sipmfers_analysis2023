#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""
Created on Wed Apr  5 14:49:28 2023

@author: utente
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os
import sys
from parsingBinary import *

#%% Import dati


if len(sys.argv) < 2: 
    nRun = 274
else:
    nRun = sys.argv[1]
    

data = openBinary(nRun)


#%% Istogramma


fig, ax = plt.subplots(2,2)
fig.set_size_inches(12, 6)
fig.subplots_adjust(hspace = .4)

ax = ax.flatten()

fig.suptitle(f"Run {nRun} - LG")


for i in range(4):
    
    # Istogrammo i dati
    h, bins = np.histogram(data["ch_data"]["lg_pha"][:,i], range = (0, 4096), bins = 4096)
    binc = bins[:-1] + (bins[1]-bins[0])/2
    
    # Li plotto
    ax[i].plot(binc, h, ds = "steps-mid", c = "darkgreen", lw = 2, label = "Spettro")
    ax[i].fill_between(binc, h, step = "mid", color = "lime", alpha = 1)

    ax[i].set_xlabel("Energy [ADC]")
    ax[i].set_ylabel("Counts")
    ax[i].set_title(f"CH{i}")
    
    ax[i].set_yscale("log")
    ax[i].grid()
    
    
plt.show()


fig, ax = plt.subplots(2,2)
fig.set_size_inches(12, 6)
ax = ax.flatten()
fig.subplots_adjust(hspace = .4)


fig.suptitle(f"Run {nRun} - HG")


for i in range(4):
    
    # Istogrammo i dati
    h, bins = np.histogram(data["ch_data"]["hg_pha"][:,i], range = (0, 4096), bins = 4096)
    binc = bins[:-1] + (bins[1]-bins[0])/2
    
    # Li plotto
    ax[i].plot(binc, h, ds = "steps-mid", c = "darkgreen", lw = 2, label = "Spettro")
    ax[i].fill_between(binc, h, step = "mid", color = "lime", alpha = 1)

    ax[i].set_xlabel("Energy [ADC]")
    ax[i].set_ylabel("Counts")
    ax[i].set_title(f"CH{i}")
    
    ax[i].set_yscale("log")
    ax[i].grid()
    
    
plt.show()
    
