#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 31 09:29:33 2023

@author: utente
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os
import sys
from parsingBinary import *


#%% Import dati
print(len(sys.argv))
for i,e in enumerate(sys.argv):
    print(i, e, type(e))



try:
    nRun = int(sys.argv[1])
    ch = int(sys.argv[2])
    numCh = int(sys.argv[3])
except:
    nRun = 1153
    ch = 0    
    numCh = 0

if ch > numCh: 
    raiseError("sei discalculico") 

data = openBinary(nRun, numCh)


#%% Istogramma


fig, ax = plt.subplots()
fig.set_size_inches(12, 6)


fig.suptitle(f"Run {nRun} - LG")

# Istogrammo i dat
try: 
    vect = data["ch_data"]["lg_pha"][:, ch]
    
except:
    vect = data["ch_data"]["lg_pha"]
    

h, bins = np.histogram(vect, range = (0, 8192), bins = 8192)
binc = bins[:-1] + (bins[1]-bins[0])/2

# Li plotto
ax.plot(binc, h, ds = "steps-mid", )#c = "darkgreen", lw = 2, label = "Spettro")
#ax.fill_between(binc, h, step = "mid", color = "lime", alpha = 1)

ax.set_xlabel("Energy [ADC]")
ax.set_ylabel("Counts")
ax.set_title(f"CH{ch}")

ax.set_yscale("log")
ax.grid()


#plt.show()


fig, ax = plt.subplots()
fig.set_size_inches(12, 6)

fig.suptitle(f"Run {nRun} - HG")

# Istogrammo i dati
#h = np.bincount( data["ch_data"]["hg_pha"][:,ch] )
h, bins = np.histogram(data["ch_data"]["hg_pha"], range = (0, 8192), bins = 8192)
binc = bins[:-1] + (bins[1]-bins[0])/2

# Li plotto
ax.plot( h,) # ds = "steps-mid", )#c = "darkgreen", lw = 2, label = "Spettro")
#ax.fill_between(binc, h, step = "mid", color = "lime", alpha = 1)

ax.set_xlabel("Energy [ADC]")
ax.set_ylabel("Counts")
ax.set_title(f"CH{ch}")
ax.set_yscale("log")
ax.grid()

    
plt.show()
    
