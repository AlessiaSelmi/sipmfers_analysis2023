#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 10:38:48 2023

@author: utente
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os, sys

#plt.ion()

#%% 

if len(sys.argv) < 2: 
    nRun = 274
    ch = 0
else:
    nRun = sys.argv[1]
    ch = sys.argv[2]    
    


print(nRun, ch)


TipoGain = "HG" # ==> Dark

fig, ax = plt.subplots(figsize = (12,6))

fig.suptitle(f"Run {nRun}")
stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_{ch}.txt"
d =np.loadtxt(os.path.join("../data", stringa1))
ax.plot(d)
ax.grid()
ax.set_title(f"CH{ch} - {TipoGain}")
ax.set_xlabel("PH [ADC]")
ax.set_yscale('log')
 
#plt.show()


TipoGain = "LG" 

data_s = []
fig, ax = plt.subplots(figsize = (12,6))
fig.suptitle(f"Run {nRun}")

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_{ch}.txt"
d = np.loadtxt(os.path.join("../data", stringa1))
data_s.append(d)
ax.plot(d)
ax.grid()
ax.set_title(f"CH{ch} - {TipoGain}")
#ax[i].set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")
ax.set_yscale('log')
    
plt.show()



