#!/usr/bin/env python3


#%% 
import argparse
#%matplotlib ipympl
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os
from parsingBinary import *

#%% 
parser = argparse.ArgumentParser(
                    prog='quickLook',
                    description='What the program does',
                    epilog='Text at the bottom of help')


parser.add_argument("filename")           # positional argument

parser.add_argument("-c", "--channel", 
		dest = "channelNumber", default = 0, type = int,
		help = "Numero di canale da plottare")

parser.add_argument("-l", "--length", 
		dest = "dataLength", default = 1, type = int,
		help = "Numero totale di canali acquisiti")

parser.add_argument("-s",
                    "--StormLike",
                    action = "store_true", 
                    help = 'Facciamo 4 plot assieme?')

parser.add_argument("--LG",
                    "--LowGain",
                    "-1",
                    dest = 'LowGain',
                    action = "store_true", 
                    help = 'Plotta LG')

parser.add_argument("--HG",
                    "--HighGain",
                    "-2",
                    dest = 'HighGain',
                    action = "store_true", 
                    help = 'Plotta HG')

parser.add_argument("--ascii",
                    "--UseAscii",
                    "-a",
                    dest = 'UseAscii',
                    action = "store_true", 
                    help = 'Usa file ascii (istogrammi)')

parser.add_argument("--log",
                    "-y",
                    dest = 'logScale',
                    action = "store_true", 
                    help = 'Scala log')

parser.add_argument("--savePlot",
                    "--save",
                    "-p",
                    dest = 'savePlot',
                    action = "store_true", 
                    help = 'Salva il plot')

args = parser.parse_args()


# print(f"Numero del file da plottare: {args.filename}")
# print(f"Numero canale da plottare: {args.channelNumber}")
# print(f"Numero totale di canali acquisiti: {args.dataLength}")
# if args.StormLike:
#       print("Plotto 4 canali")
# if args.HighGain:
#       print("Plotto HG")
# if args.LowGain:
#       print("Plotto LG")
# if args.UseAscii:
#        print("Uso i file ascii")
# if args.logScale:
#       print("Plotto in scala logaritmica")

if args.channelNumber >= args.dataLength:
	raise Exception(f"ERRORE PROIBITO!!! ")

# %%

# Variabili hardcodate

Fondoscala = 8192
bordiBin = np.arange(-.5, Fondoscala + 1.5, 1)
numBoard = 0

# Se voglio usare i binari
if not args.UseAscii:

      # Se sono in modalità storm
      if args.StormLike:
            # if args.dataLength < 4: 
            #       raise Exception("Non ci sono veramente 4 canali") # Pensarci


            # Carico i dati usando DI DEFAULT 
            # TODO: generalizzare che posso bypassare
            data = openBinary(args.filename, 4)

            
            if args.HighGain:

                  # Definisco liste contenenti gli istogrammi
                  h_HG = []
                  binc_HG = []

                  # Ciclo sui quattro canali
                  for i in range(4):

                        # Carico il vettore relativo al canale i-esimo
                        vect = data["ch_data"]["hg_pha"][:, i]

                        # Istogrammo i dati
                        _h_HG, _bins_HG = np.histogram(vect, bins = bordiBin) # Verificare che sia giuusto
                        _binc_HG = _bins_HG[:-1] + (_bins_HG[1]-_bins_HG[0])/2

                        # Esporto i valori nelle liste finali
                        h_HG.append(_h_HG)
                        binc_HG.append(_binc_HG)


            if args.LowGain:

                  # Definisco liste contenenti gli istogrammi
                  h_LG = []
                  binc_LG = []

                  # Ciclo sui quattro canali
                  for i in range(4):

                        # Carico il vettore relativo al canale i-esimo
                        vect = data["ch_data"]["lg_pha"][:, i]

                        # Istogrammo i dati
                        _h_LG, _bins_LG = np.histogram(vect, bins = bordiBin) # Verificare che sia giuusto
                        _binc_LG = _bins_LG[:-1] + (_bins_LG[1]-_bins_LG[0])/2

                        # Esporto i valori nelle liste finali
                        h_LG.append(_h_LG)
                        binc_LG.append(_binc_LG)

      # Se sono in modalità canale singolo
      else:

            # Carico i dati usando le impostazioni
            data = openBinary(args.filename, args.dataLength)


            # Se voglio avere l'HG
            if args.HighGain:
                  try: 
                        vect = data["ch_data"]["hg_pha"][:, args.channelNumber]
    
                  except:
                        vect = data["ch_data"]["hg_pha"]
                  
                  h_HG, bins_HG = np.histogram(vect, bins = bordiBin) # Verificare che sia giuusto
                  binc_HG = bins_HG[:-1] + (bins_HG[1]-bins_HG[0])/2

            # Se voglio avere il LG
            if args.LowGain:
                  try: 
                        vect = data["ch_data"]["lg_pha"][:, args.channelNumber]
    
                  except:
                        vect = data["ch_data"]["lg_pha"]
                  
                  h_LG, bins_LG = np.histogram(vect, bins = bordiBin)
                  binc_LG = bins_LG[:-1] + (bins_LG[1]-bins_LG[0])/2





# Se voglio usare gli ascii
else:

      # Se sono in modalità Storm
      if args.StormLike:

            if args.HighGain:
                  # Definisco liste contenenti gli istogrammi
                  h_HG = []
                  binc_HG = []

                  # Ciclo sui quattro canali
                  for i in range(4):

                        # Carico il vettore relativo al canale i-esimo
                        stringa = f"Run{args.filename}_PHA_HG_{numBoard}_{4*i}.txt"
                        d = np.loadtxt(os.path.join("../data", stringa))


                        # Esporto i valori nelle liste finali
                        h_HG.append(d)
                        binc_HG.append( np.arange(0,Fondoscala, 1))

            if args.LowGain:
                  # Definisco liste contenenti gli istogrammi
                  h_LG = []
                  binc_LG = []

                  # Ciclo sui quattro canali
                  for i in range(4):

                        # Carico il vettore relativo al canale i-esimo
                        stringa = f"Run{args.filename}_PHA_LG_{numBoard}_{4*i}.txt"
                        d = np.loadtxt(os.path.join("../data", stringa))


                        # Esporto i valori nelle liste finali
                        h_LG.append(d)
                        binc_LG.append( np.arange(0,Fondoscala, 1))



      else: # Se sono in modalità canale singolo
            if args.HighGain:
                  # Carico i dati
                  stringa = f"Run{args.filename}_PHA_HG_{numBoard}_{args.channelNumber*4}.txt"
                  d = np.loadtxt(os.path.join("../data", stringa))
                  
                  # Preparo i vettori per gli istogrammi
                  h_HG = d
                  binc_HG = np.arange(0,Fondoscala, 1)

            if args.LowGain:
                  # Carico i dati
                  stringa = f"Run{args.filename}_PHA_LG_{numBoard}_{args.channelNumber*4}.txt"
                  d = np.loadtxt(os.path.join("../data", stringa))
                  
                  # Preparo i vettori per gli istogrammi
                  h_LG = d
                  binc_LG = np.arange(0,Fondoscala, 1)
            






#%% Plotto gli istogrammi


def plottaSingolo(binc, h, tipoGain):
      fig, ax = plt.subplots()

      ax.plot(binc, h, ds = "steps-mid", c = "tab:green")
      if args.logScale:
            ax.set_yscale("log")
      ax.set_xlabel("PH [ADC]")
      ax.set_ylabel("Counts")
      ax.set_title(f"Run {args.filename} - Ch {args.channelNumber} - {tipoGain}")
      ax.grid()

      # plt.show()

      if args.savePlot:
            nomeFile = f"plot_{args.filename}_ch{args.channelNumber}_{tipoGain}"
            fig.savefig(f"./SavedPlots/{nomeFile}.png")

      return

def plottaStorm(binc, h, tipoGain):
      fig, ax = plt.subplots(2,2)
      ax = ax.flatten()
      fig.subplots_adjust(hspace = .5)

      fig.suptitle(f"Run {args.filename} - {tipoGain}")

      for i in range(4):
            ax[i].plot(binc[i], h[i], ds = "steps-mid", c = "tab:green")
            ax[i].set_title(f"Channel 0{i}")

            ax[i].set_xlabel("PH [ADC]")
            ax[i].set_ylabel("Counts")
            ax[i].grid()

            if args.logScale:
                  ax[i].set_yscale("log")
      # plt.show()

      if args.savePlot:
            nomeFile = f"plot_{args.filename}_StormLike_{tipoGain}"
            fig.savefig(f"./SavedPlots/{nomeFile}.png")
      return 


if args.StormLike:
      if args.HighGain:
            plottaStorm(binc_HG, h_HG, "HG")
      if args.LowGain:
            plottaStorm(binc_LG, h_LG, "LG")
else:
      if args.HighGain:
            plottaSingolo(binc_HG, h_HG, "HG")
      if args.LowGain:
            plottaSingolo(binc_LG, h_LG, "LG")


plt.show()

