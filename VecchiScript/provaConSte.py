# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 14:23:34 2023

@author: utente
"""


"""
Config canale

Dunque, quelli che vedi sono gli spettri delle PH. 
Il canale 4 della board 1 è il SiPM hamamatsu dei pavesi, 15 um di passo, 1.3 x 1.3 mm². 
Della board 0 va guardato solo quello. 
Della board 1 ho 4 canali, ogni canale è un SiPM dei 4 della SensL

"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os

#%% Run 186
"""

Run 1876
Bias : Hama => 46, sensL => 28

"""
nRun = 186

TipoGain = "HG" # ==> Dark
bias_h = 46
bias_s = 28


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain} - Run {nRun}")
#ax.set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()
fig.suptitle(f" Run : {nRun}")

for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    ax[i].set_xlim(0,1000)
    ax[i].set_xlabel("PH [ADC]")
    
    
TipoGain = "LG" # ==> Dark


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain} - Run {nRun}")
#ax.set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()
fig.suptitle(f" Run : {nRun}")

for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    #ax[i].set_xlim(0,3500)
    ax[i].set_xlabel("PH [ADC]")
    
#%% Run 187
"""

Run 187 
Bias : Hama => 44, sensL => 32

"""
nRun = 187

TipoGain = "HG" # ==> Dark
bias_h = 44
bias_s = 32


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain} - Run {nRun}")
#ax.set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()
fig.suptitle(f" Run : {nRun}")

for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    #ax[i].set_xlim(0,3500)
    ax[i].set_xlabel("PH [ADC]")
    
    
TipoGain = "LG" # ==> Dark


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain} - Run {nRun}")
#ax.set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()
fig.suptitle(f" Run : {nRun}")

for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    #ax[i].set_xlim(0,3500)
    ax[i].set_xlabel("PH [ADC]")
    

#%% Run 188
"""

Run 188
Bias : Hama => 48, sensL => 30

"""
nRun = 188

TipoGain = "HG" # ==> Dark
bias_h = 48
bias_s = 30


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain} - Run {nRun}")
ax.set_xlim(0,2000)
ax.set_xlabel("PH [ADC]")


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()
fig.suptitle(f" Run : {nRun}")
for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    ax[i].set_xlim(0,2000)
    ax[i].set_xlabel("PH [ADC]")
    
    
TipoGain = "LG" # ==> Dark


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain} - Run {nRun}")
#ax.set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()

fig.suptitle(f" Run : {nRun}")
for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    #ax[i].set_xlim(0,3500)
    ax[i].set_xlabel("PH [ADC]")
    
