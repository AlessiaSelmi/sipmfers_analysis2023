#!/bin/bash


# Mi assicuro che venga passato il file di input
# TODO: Passare solo il numero
if [ $# -ne 1 ]; then
  echo "Usage: $0 <filename>"
  exit 1
fi


# Settings
skipRows=9
numBoards=4



# Nome del file di input
input_file=$1

# Nome del file di output
output_file_HG="${input_file/Run/"parsed_HG_Run"}"
output_file_LG="${input_file/Run/"parsed_LG_Run"}"


echo "Convertirò ${input_file} --> ${output_file_HG}"
echo "Convertirò ${input_file} --> ${output_file_LG}"


# Se esistono già i files li rimuovo
# Potrei averli convertiti a run in corso, e i files essere a metà
if [ -e $output_file_HG ]; then
    rm -fv $output_file_HG
fi

if [ -e $output_file_LG ]; then
    rm -fv $output_file_LG
fi




# Inizializza il contatore di righe
line_count=0

# Inizializza la variabile che contiene gli ultimi valori delle righe
last_values_HG=""
last_values_LG=""


echo "Sto iniziando..."
# Legge il file di input riga per riga
while read line; do

    # Salto le righe di header
    if (($line_count < $skipRows)); then
        line_count=$((line_count+1))
        continue
    fi


    # Estrae l'ultimo valore della riga
    last_word_HG=$(echo $line | awk '{print $NF}')
    last_word_LG=$(echo $line | awk '{print $(NF-1)}')



    # Aggiunge l'ultimo valore alla stringa degli ultimi valori
    last_values_HG+="$last_word_HG "
    last_values_LG+="$last_word_LG "


    # Incrementa il contatore di righe
    line_count=$((line_count+1))


    # Se si è raggiunto un gruppo di quattro righe
    if (( ($line_count-$skipRows) % 4 == 0 )); then

        # Scrive la stringa degli ultimi valori nel file di output
        echo "$last_values_HG" >> "$output_file_HG"
        echo "$last_values_LG" >> "$output_file_LG"

        # Resetta la stringa degli ultimi valori
        last_values_HG=""
        last_values_LG=""

    fi

done < $input_file

# Se ci sono ancora righe rimanenti che non formano un gruppo di quattro
if (( $line_count % 4 != 0 )); then

    # Scrive la stringa degli ultimi valori rimanenti nel file di output
    echo "$last_values_HG" >> "$output_file_HG"
    echo "$last_values_LG" >> "$output_file_LG"
fi


echo "Ho finito :)"
