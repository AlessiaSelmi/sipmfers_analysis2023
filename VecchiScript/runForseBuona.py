# -*- coding: utf-8 -*-
"""
Created on Mon Mar 27 11:41:35 2023

@author: utente
"""



import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os


#%% Run 155
"""

Run 155
Bias : Hama => 46, sensL => 28

"""
nRun = 155

TipoGain = "HG" # ==> Dark
bias_h = 48
bias_s = 30


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain}")
ax.set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")
#ax.set_yscale('log')

# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()

for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    ax[i].set_xlim(0,1000)
    ax[i].set_xlabel("PH [ADC]")
    #ax[i].set_yscale('log')
    
"""
    
TipoGain = "LG" # ==> Dark


# Hamamatsu ==> Board 0, CH 4

stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_4.txt"
data_hama = np.loadtxt(os.path.join("data", stringa1))

fig, ax = plt.subplots(figsize = (12,6))
ax.plot(data_hama)
ax.grid()
ax.set_title(f"Hamamatsu - Bias: {bias_h} - {TipoGain}")
#ax.set_xlim(0,1000)
ax.set_xlabel("PH [ADC]")


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_sensL = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()

for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_1_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_sensL.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    #ax[i].set_xlim(0,3500)
    ax[i].set_xlabel("PH [ADC]")
"""

