#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 18:52:26 2023

@author: stefano
"""

# Le righe della board 0 sono 10, 14, 18...


import numpy as np
import sys, os, re, glob

file = "./data/Run267_list.txt"

skipRows = 9
numBoards = 4


#%% Ottengo il numero di righe
numLines = sum(1 for l in open(file, "r"))


# Prealloco la matrice
datiLG = np.empty( (int((numLines - skipRows)/4), numBoards), int)
datiHG = np.empty( (int((numLines - skipRows)/4), numBoards), int)



#%%
with open(file, "r") as f:
    for i, l in enumerate(f):
        
        # Salto le prime righe di header
        if i < skipRows: continue
    
    
        # Indice progessivo sulle righe vere
        realIdx = i-skipRows
        
        # Numero di evento
        nEv = int(np.floor(realIdx % numBoards))
        
        # Numero di board associata all'evento realIdx
        nBoard = realIdx % numBoards
        
        
        tmpLine = re.split("\s+", l.strip())
        datiLG[nEv, nBoard] = tmpLine[-2]
        datiHG[nEv, nBoard] = tmpLine[-1]
    
    
    
    
    
    
    
    