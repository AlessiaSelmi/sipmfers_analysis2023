# -*- coding: utf-8 -*-
"""
Created on Mon Mar 27 15:18:08 2023

@author: utente
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
import os

#%% Run 216
"""

Run 216
Bias : sensL => 32
GAIN CAMBIATO! 
Autotrigger!
No hamamatsu
"""
nRun = 216

TipoGain = "HG" # ==> Dark
bias_s = 32


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_s = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()
fig.suptitle(f"Run {nRun}")
for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_s.append(d)
    ax[i].plot(d)
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    ax[i].set_xlim(0,1000)
    ax[i].set_xlabel("PH [ADC]")

#%%Fit Run217

#Fittiamo


def gaus4(x, a1,a2,a3,a4, mu1,mu2,mu3, mu4, sigma1,sigma2,sigma3, sigma4):
    y =  a1*np.exp(-(x-mu1)**2/(2*sigma1**2)) +a2*np.exp(-(x-mu2)**2/(2*sigma2**2)) + a3*np.exp(-(x-mu3)**2/(2*sigma3**2)) + a4*np.exp(-(x-mu4)**2/(2*sigma4**2)) 
    return y

start_fit = [40, 40, 50]
stop_fit = [300, 300, 270]

fpara1 = [0.9e5, 1.9e5, 1.3e5, 0.8e5,
         55, 140, 240, 200,
         15, 25, 50, 500]

fpara2 =  [0.4e5, 1.6e5, 1.5e5, 0.8e5,
         65, 140, 240, 200,
         8, 25, 50, 500]
fpara = [fpara1, fpara1, fpara2]
optPars = []
pCov = []

fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax=ax.flatten()

for i in range(3):  
    
    xfit = np.arange(start_fit[i],stop_fit[i])

    yfit = data_s[i][start_fit[i]:stop_fit[i]]
    ax[i].plot(xfit, yfit, label = 'Data')
    #ax[i].plot(xfit, gaus4(xfit,*fpara[i]))
    oP, pC = opt.curve_fit(gaus4, xfit, yfit, sigma=np.sqrt(yfit), absolute_sigma=True, p0 = fpara[i])
    ax[i].plot(xfit, gaus4(xfit,*oP),'--', label = f"Fit - mu1 = {oP[4]:.2f}, mu2 = {oP[5]:.2f}, \n mu3 = {oP[6]:.2f}")
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    ax[i].set_xlabel("PH [ADC]")
    optPars.append(oP)
    pCov.append(pC)
    ax[i].legend()
    

ax[3].plot(data_s[3])
ax[3].set_xlim(0,1000)
ax[3].grid()
ax[3].set_title(f"SensL CH{3}  - Bias: {bias_s} - {TipoGain}")
ax[3].set_xlabel("PH [ADC]")


for i in range(3):
    
    print(f"CH{i}")
    print(f"  --> mu1 = {optPars[i][4]:.2f} ± {np.sqrt(np.diag(pCov[i]))[4]:.2f} , mu2 = {optPars[i][5]:.2f} ± {np.sqrt(np.diag(pCov[i]))[5]:.2f}, mu3 = {optPars[i][6]:.2f} ± {np.sqrt(np.diag(pCov[i]))[6]:.2f}")
    print(f"  --> mu2-mu1 = {optPars[i][5]-optPars[i][4]:.2f} ± {np.sqrt(np.diag(pCov[i])[5]+np.diag(pCov[i])[4]):.2f}; mu3-mu2 = {optPars[i][6]-optPars[i][5]:.2f} ± {np.sqrt(np.diag(pCov[i])[6]+np.diag(pCov[i])[5]):.2f}")
    
    
#%% Confronto Run 216-217
"""

Run 217
Bias : sensL => 32
GAIN CAMBIATO! 
Trigger esterno!
No hamamatsu
"""
nRun = 217

TipoGain = "HG" # ==> Dark
bias_s = 32


# SensL ==> Board 1, CH0, CH1, CH2, CH3

data_s1 = []
fig, ax = plt.subplots(2,2, figsize = (12,6))
fig.subplots_adjust(hspace = .4)
ax = ax.flatten()
fig.suptitle("Confronto Run216 (autotrigger) - Run217 (trigger esterno)")
for i in range(4):
    stringa1 = f"Run{nRun}_PHA_{TipoGain}_0_{i*4}.txt"
    d =np.loadtxt(os.path.join("data", stringa1))
    data_s1.append(d)
    ax[i].plot(d/np.sum(d), label = 'Trigger esterno - Run 217')
    ax[i].plot(data_s[i]/np.sum(data_s[i]), label = 'Autotrigger - Run 216')
    ax[i].grid()
    ax[i].set_title(f"SensL CH{i}  - Bias: {bias_s} - {TipoGain}")
    ax[i].set_xlim(0,1000)
    ax[i].set_xlabel("PH [ADC]")    
    ax[i].legend()
    if i==3: continue

    ax[i].axvline(optPars[i][4], ls = '--', c = 'gray')
    ax[i].axvline(optPars[i][5], ls = '--', c = 'gray')
    ax[i].axvline(optPars[i][6], ls = '--', c = 'gray')
    
    
    